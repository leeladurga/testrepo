def publishDockerImages() {
        textParam( name: 'DEV_FEATURES', value: getDeveloperOverrideAsList( 'devFeatures' ).join( '\n' ) ),
        booleanParam( name: 'BUILD_PREINSTALLED_IMAGES', value: true ),
        booleanParam( name: 'REGIONAL_PUBLISHING', value: isProductionBranch() ),
        booleanParam( name: 'DEPLOY_TO_KUBERNETES', value: shouldDeployToKubernetes() ),
        booleanParam( name: 'PROVISION_ULTRA_STACK', value: !DEVELOPER_OVERRIDES[ 'deployWithoutUltra' ] )